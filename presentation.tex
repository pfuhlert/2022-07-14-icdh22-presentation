\documentclass[aspectratio=1610, xcolor={dvipsnames}]{beamer}

\usepackage{amsmath,amsfonts,amsthm,amssymb,bm}

\usepackage[backend=biber, style=numeric, citestyle=numeric, sorting=none]{biblatex}
\addbibresource{library.bib}
\AtBeginBibliography{\scriptsize}

\usepackage{booktabs}
\usepackage[english]{babel}
\usepackage{caption}
\usepackage{csquotes}
\usepackage{fontawesome}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{romannum}
\usepackage{siunitx}
\DeclareSIUnit\year{\text{year}}
\DeclareSIUnit\month{\text{month}}
\usepackage{standalone}
\usepackage{subfig}
\usepackage{tikz}
\usepackage{bbm}

\usetikzlibrary{arrows, arrows.meta, fit, calc, matrix,positioning, chains, decorations.pathreplacing, shapes}

\usepackage{standalone}

\usepackage{pgfplots}

\usepackage[normalem]{ulem}
\usepackage{bm}

% FONTS

\usepackage[mathrm=sym]{unicode-math}
\usepackage[sfdefault]{Fira Sans}
\setmathfont{Fira Math}
% \setmathfont{XITS Math}[range={\mathscr,\mathbfscr}]
% \setmathfont{XITS Math}[range={\mathcal,\mathbfcal},StylisticSet=1]

% END FONTS

% DATASETS
\newcommand{\support}{SUPPORT}
\newcommand{\metabric}{METABRIC}
\newcommand{\flchain}{FLCHAIN}
\newcommand{\mkpca}{MKPCa}

% SCORERS
\newcommand{\cindex}{Concordance Index}
\newcommand{\cindexabb}{C-index}
\newcommand{\cindextd}{Time-Dependent Concordance Index}
\newcommand{\cindextdabb}{C-index-td}
\newcommand{\brier}{Brier Score}
\newcommand{\intbrier}{Integrated Brier Score}
\newcommand{\cdauc}{Cumulative-Dynamic AUROC}
\newcommand{\cdaucabb}{CDAUC}
\newcommand{\ddc}{Distributional Divergence for Calibration}
\newcommand{\ddcabb}{DDC}

\newcommand{\lkernelours}{$\mathcal{\tilde{L}}_{kernel}$}
\newcommand{\lrps}{$\mathcal{L}_{\text{RPS}}$}

% MODELS
\newcommand{\coxph}{CoxPH}
\newcommand{\deepsurv}{DeepSurv}
\newcommand{\coxtime}{CoxTime}
\newcommand{\drsa}{DRSA}
\newcommand{\kamran}{Kamran}


% ### Experimental ###


% ### End Experimental ###

% ### Theme ###

\usetheme[progressbar=frametitle,
subsectionpage=progressbar,
block=fill]{metropolis}

\metroset{subsectionpage=progressbar, numbering=fraction, titleformat=smallcaps}
\vspace*{-2em}
\title{\Large{Deep Learning-Based Discrete Calibrated Survival Prediction}}
\date{July 14th, 2022}
\author{P. Fuhlert, A. Ernst, E. Dietrich, F. Westhaeusser, K. Kloiber, S. Bonn}
\institute{Institute of Medical Systems Biology, Center for Biomedical AI (bAIome) \\
Center for Molecular Neurobiology Hamburg (ZMNH)\\
University Medical Center Hamburg-Eppendorf (UKE)
}
\makeatletter
\input{metropolis_extensions.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\begin{frame}[noframenumbering]
\titlepage
\thispagestyle{empty}
\tikz [remember picture,overlay]
\node[anchor=east] (zmnh) at
    ([xshift=-2.5cm, yshift=1.5cm]current page.south east)
    {\includegraphics[width=0.2\textwidth]{img/zmnh-logo.png}};

\tikz [remember picture,overlay]
\node[anchor=east] (uke) at
    ([xshift=-1cm, yshift=1.5cm]current page.south east)
    {\includegraphics[width=0.07\textwidth]{img/uke-logo.png}};

\tikz [remember picture,overlay]
\node[anchor=east] (baiome) at
    ([left of=zmnh, xshift=-5cm, yshift=1.5cm]current page.south east)
    {\includegraphics[height=0.1\textheight]{img/logo-baiome.png}};

\end{frame}

\begin{frame}{Overview}
\tableofcontents

\end{frame}

\begin{frame}{Overview}

\tableofcontents

\end{frame}

\section{Survival Analysis}

\begin{frame}{Survival Analysis}

  \begin{figure}[]
    \centering
    \includegraphics<1>[height=.5\textheight]{img/survival-curve.png}
    \includegraphics<2->[height=.5\textheight]{img/survival-curve-2.png}
  \end{figure}

\begin{itemize}
    \item Estimate survival probability over time
    \item<2-> Identify covariates that influence Survival (e.g. $x_0 =$ "active smoker")
    \item<3-> Applications: Survival, customer churning, mechanical failure
\end{itemize}


\end{frame}

\begin{frame}{Survival Analysis \Romannum{2} --- Censoring}

\begin{figure}
    \centering
    \includegraphics[width=.6\textwidth]{tikz/censoring.tex}
\end{figure}

\begin{itemize}
\item Typical for e.g. medical studies, not all included patients participate a study from start to end
\item If they only participate in parts of a study, their data is considered \alert{censored}
\end{itemize}
\end{frame}

\begin{frame}{Discrimination and Calibration}

\begin{figure}
    \centering
    \includegraphics[width=.9\textwidth]{img/survival-curve-obs-est.png}
\end{figure}

\begin{itemize}
    \item Typically, Survival analysis aims to identify covariates that predict survival
    \item Discrimination: evaluate the \alert{order} of predictions compared to event durations
    \item<2> Both predictions yield high discrimination (order), but right prediction should be preferred
\end{itemize}

\end{frame}

\section{Methods}

\subsection{Metrics}

\begin{frame}{Evaluation Metrics}
\begin{description}[leftmargin=1cm]
    \item[Discrimination] Sort patients correctly. Which covariate has the
    worst/best influence on survival
    \item[] Measured by \cindexabb{} \cite{Harrell1982} and \cdaucabb{} \cite{Blanche2019}
    \item[Calibration] Give a reasonable individual survival probability
    \item[] Measured by \ddcabb{} \cite{Kamran2021}
  \end{description}
\end{frame}

\subsection{Datasets}

\begin{frame}{Datasets Overview}
\begin{table}[b]
\begin{center}
\begin{tabular}{lrrr}
 & \support{} & \metabric{} & \flchain{} \\
\midrule
 \#patients & 8873 & 1904 & 7874 \\
 \#features & 14 & 9 & 8 \\
 \alert<2>{censoring rate} & \SI{32}{\percent} & \SI{42}{\percent} & \SI{72}{\percent} \\
median survival time & \SI{57}{days} & \SI{85}{months} & \SI{71}{months} \\
\alert<3>{prop. hazards violated} & \SI{79}{\percent} & \SI{56}{\percent} & \SI{25}{\percent}
\end{tabular}
\end{center}
\end{table}
\end{frame}

\begin{frame}{Datasets --- Example --- \support{}}
\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{|rrlrr|rr|}
\hline
age & diabetes & ... & serum\_creatinine & respiration\_rate & event\_days & has\_event \\\hline
64  & 1        &     & 0.5               & 24                & 23          & 1          \\
51  & 0        &     & 0.6               & 29                & 466         & 1          \\
39  & 0        &     & 1.0               & 24                & 529         & 0          \\
67  & 0        &     & 1.9               & 28                & 105         & 1          \\
... & & & & & &\\ \hline
\end{tabular}
}
    \caption*{\support{} \cite{Knaus1995} -- time to death for seriously ill hospitalized patients}
\end{table}
\end{frame}

\subsection{Models}

\begin{frame}{Baselines}
Cox Proportional Hazards Model \cite{Cox1972}
    \begin{itemize}
        \item Proportional Hazards Assumption: linear, independent, time-invariant features
        \item Focus on covariate impact (e.g. hazard ratio)
        \positem Easy to interpret
        \negitem Assumptions often not met in practice
    \end{itemize}

    % \begin{figure}
    %     \includegraphics[height=.4\textheight]{img/coxph-sample-curves.png}
    %     \caption*{Sample survival curves for 5 random \support{} individuals}
    % \end{figure}
\end{frame}


\begin{frame}{Baselines \Romannum{2}}
\begin{description}
    \item<1->[\deepsurv{}] Allow \alert{non-linear} interactions of covariates with neural network encoder \cite{Katzman2018}
    \item<2->[\coxtime{}] Assume \alert{time-variant} influence of covariates by including time as an additional encoder input \cite{Kvamme2019}
    \item<2->[] Consequently allows for crossing survival curves
\end{description}
\end{frame}
\begin{frame}{Baselines \Romannum{3} --- Discrete NN Survival Models}
\begin{minipage}{.99\textwidth}
  \begin{description}
    \item Predict survival probability at discrete timesteps $t_l$
    \item[$\rightarrow$] Multiple dependent classification problems
    \item[\drsa{}]<2-> Exploit sequential dependency using a recurrent neural network decoder \cite{Ren2019}
    \item[\kamran{}]<3-> Additionally accounts for proper calibration \cite{Kamran2021}
  \end{description}
\end{minipage}
% \begin{minipage}{.3\textwidth}
%     \begin{figure}
%         \includegraphics<1->[width=\textwidth]{tikz/mlp-survival-architecture.tex}
%     \end{figure}
% \end{minipage}
\end{frame}

\subsection{Discrete Calibrated Survival (DCS)}

\begin{frame}{Discrete Calibrated Survival (DCS)}

\begin{minipage}{.49\textwidth}
\begin{itemize}
    \item Extends ideas from \drsa{} and \kamran{}
    \item<2-> Use FCNN as feature encoder
    \item<3-> Exploit sequential dependency by LSTM architecture
    \item<4-> Aggregation of hazard rates $h_l$ ensures monotonically decreasing output: \(\hat{S}(t_l | x_i)=\prod_{j=1}^l (1-h_j)\)
\end{itemize}

\end{minipage}
\begin{minipage}{.5\textwidth}
    \begin{figure}
        \centering
        \includegraphics[width=\textwidth]{tikz/model_dcs_architecture.tex}
    \end{figure}
\end{minipage}
\end{frame}

\begin{frame}{DCS --- Temporal Output Node Spacing}
\begin{minipage}{.5\textwidth}
    \begin{itemize}
        \item How to discretize continuous output space $t$?
        \item Typically linear (equidistant) spacing in time
        \item<2-> Investigate linear-, logarithmic- and \alert<2>{quantile-} (same number of events per node) spacing
    \end{itemize}
\end{minipage}
\hfill
\begin{minipage}{.4\textwidth}
    \begin{figure}
        \centering
        \only<2>{
            \includegraphics<2->[width=\textwidth]{img/output_grids_support.png}
            \caption*{Histograms of the number of events per temporal prediction for \support{}.}
        }
    \end{figure}
\end{minipage}
\end{frame}

\begin{frame}{DCS --- Objective Function Visualization}
\begin{figure}
    \centering
    \includegraphics[width=.55\textwidth]{tikz/model_dcs_loss_visu.tex}
\end{figure}
\begin{description}
    \item<2->[{\color{red}{$\mathcal{L}_\text{cal}$}}] Punish predictions $<1$ before event or censoring and $>0$ after the event time
    \item<4->[{\color{orange}{$\mathcal{L}_\text{disc}$}}] "Push away" predictions where individuals survived longer
\end{description}
\only<5->{$\rightarrow$ Weight calibration and discrimination: {\color{red}{$\mathcal{L}_\text{cal}$}} $\, +\,\lambda$ \color{orange}{$\mathcal{L}_\text{disc}$}}
\end{frame}

\begin{frame}{DCS  --- Discriminative comparisons}
\begin{figure}
    \centering
    \includegraphics<2>[height=.5\textheight]{img/comparisons_noresample.png}
\end{figure}
\begin{itemize}
    \item How to decide which predictions to "push away"
    \item Include not only event-to-event, but event-to-censoring pairs
    \item This boosts the number of comparisons in the loss depending on the dataset's censoring rate $c$
    \item<2-> The factor can be estimated by $F_\text{est}(c) = 1 / (1-c)$
\end{itemize}
\end{frame}

\section{Results}

\begin{frame}{Discrimination Performance --- \cdaucabb{} ($\uparrow$)}

\begin{figure}
    \centering
    \begin{tikzpicture}
    \node<1-3> (cdauc) at (0,0) {\includegraphics[width=\textwidth]{img/scores_cdauc-sksurv_test.png}};
    \draw<-1>[draw=mLightBackground, fill=mLightBackground] (-2.3,-2) rectangle ++(10,6);
    \draw<-2>[draw=mLightBackground, fill=mLightBackground] (2.35,-2) rectangle ++(10,6);
    \end{tikzpicture}
\end{figure}

\begin{itemize}
    \item<3-> Best discriminative performance on all analyzed survival models
\end{itemize}

\end{frame}

\begin{frame}{Calibration performance --- \ddcabb{} ($\downarrow$)}
\begin{figure}
    \centering
    \begin{tikzpicture}
        \node<1-2> (ddc) at (0,0) {\includegraphics[width=\textwidth]{img/scores_ddc_test.png}};
        \draw<-1>[draw=mLightBackground, fill=mLightBackground] (-2.3,-2) rectangle ++(10,6);
        \draw[decorate, decoration={brace, amplitude=5pt}] (-2.7,-2) -- (-4.2,-2) node [align=center, black, midway, yshift=-1em] {\tiny{discrete output}};
    \end{tikzpicture}
\end{figure}

\begin{itemize}
    \item Classical approaches achieve generally better calibration
    \item<2-> Our approach yield improvement for discrete output NN models
\end{itemize}
\end{frame}

\section{Summary}

\begin{frame}{Conclusion}
\begin{itemize}
    \item Introduced Discrete Calibrated Survival (DCS) approach
    \positem NN survival approaches can achieve SotA discriminative performance
    \begin{itemize}
        \positem Including all possible comparisons between individuals
        \positem Focus on calibration or discrimination can be tuned by hyperparameter
        \positem Novel data-driven quantile node spacing lead to best overall results
    \end{itemize}
    \negitem Calibration in discrete NN approaches still has room for improvement
\end{itemize}
\end{frame}

\begin{frame}{Future Work}
    \begin{itemize}
        \item Apply Survival model to real-world data with clinicians
        \item Extend model for multiple modalities (e.g. images) and/or multi-task problems
        \item Analyze feature importance e.g. by the generated latent representations
    \end{itemize}
\end{frame}

\backup{}

\begin{frame}[allowframebreaks]{References}
  \printbibliography[heading=none]
\end{frame}

\begin{frame}{DCS --- Repository}

\begin{figure}
    \centering
    \includegraphics[width=.7\textwidth]{img/dcs-repo.png}\footnote{\href{https://github.com/imsb-uke/dcsurv}{github.com/imsb-uke/dcsurv}}
\end{figure}

\end{frame}

\begin{frame}{Calibration}

\begin{itemize}
    \item Discriminative performance does not evaluate absolute predicted survival probability
    \item Better calibration benefitial for the individual:
    \item[] "Your 5 year survival probability is 70\%"
    \item Calibration is another important measure for survival estimates
\end{itemize}

\end{frame}

\begin{frame}{\cindex{} \cite{Harrell1982} \cite{Antolini2005}}
  \begin{itemize}
    \item Population level metric based on correctly ordered pairs of predictions
    \item Leave out a pair if the concordance is unknown due to censoring
    \item Independent of absolute values, only \alert{order} is relevant
    \item Perfect score is \(1\), random predictions lead to \(\approx 0.5\)
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{(D-) Calibration \cite{Haider2018}}

  \begin{itemize}
    \item How reliable is individual survival curve?
    \item Survival estimate should match the underlying distribution
    \item<2-> Calculate estimated survival at event time: $\hat{S}(z_i|\mathbf{x}_i)$
    \item<2-> Distribute Individuals into 10 bins across the unit interval:
  \end{itemize}
  \begin{figure}
    \includegraphics<2->[height=.5 \textheight]{img/d-calibration.png} \footnote{Image from \cite{Haider2018}}
  \end{figure}
\end{frame}

\begin{frame}
  \frametitle{Distributional Divergence for Calibration (DDC) \kamran{}}
  \begin{figure}
    \includegraphics[width=.9\textwidth]{img/ddc-loss-visu.png}
  \end{figure}
  \begin{itemize}
    \item How reliable is individual prediction?
    \item Calculate estimated survival at event time: $\hat{S}(z_i|\mathbf{x}_i)$ and discretize in 10 buckets of the unit interval
    \item Use KL-Divergence \cite{Kullback1951} between observed buckets and uniform distribution
    \[D_{\mathrm{KL}}(P \| Q)=\sum_{x \in \mathcal{X}} P(x) \log \left(\frac{P(x)}{Q(x)}\right)\]
  \end{itemize}
\end{frame}

\begin{frame}{Datasets --- Example --- \metabric{}}
\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{|rrrl|rr|}
\hline
 marker\_EGFR &  chemotherapy &  er\_positive & ... &  event\_days &  has\_event \\
\hline
           7 &             0 &            1 &     &        3835 &          1 \\
           5 &             1 &            0 &     &        5528 &          0 \\
           5 &             0 &            1 &     &        3435 &          1 \\
           6 &             0 &            1 &     &        7891 &          0 \\
           6 &             0 &            1 &     &        6607 &          0 \\
... & & & & & \\
\hline
\end{tabular}
}
\caption*{\metabric{} \cite{curtis2012} -- long-term clinical outcome of breast cancer}
\end{table}
\end{frame}

\begin{frame}{Datasets --- Example --- \flchain{}}
\begin{table}[]
\resizebox{\textwidth}{!}{%
\begin{tabular}{|rrrrlr|rr|}
\hline
age &  sex &  kappa &  lambda & ... &  mgus &  event\_days &  has\_event \\
\hline
  53 &    1 &   0.98 &    1.06 &     &     0 &        2628 &          1 \\
  62 &    1 &   1.52 &    0.73 &     &     1 &        3859 &          1 \\
  50 &    0 &   1.02 &    1.29 &     &     0 &        4710 &          0 \\
  70 &    0 &   1.78 &    4.41 &     &     0 &        4539 &          0 \\
  81 &    0 &   1.64 &    1.69 &     &     0 &         718 &          1 \\
  ... & & & & & & & \\
\hline
\end{tabular}
}
\caption*{\flchain{} \cite{Therneau2014} -- patient mortality from blood levels}
\end{table}
\end{frame}

\begin{frame}{Baselines --- Cox Proportional Hazards Model \cite{Cox1972}}
  \begin{itemize}
    \item Consider \(h(t | x) = 1 - S(t | x)\) to be called the hazard function
    \item Split hazard function in base hazard and influence of covariates
    \[h(t | x) = h_0(t) \cdot g(\mathbf{x})\]
    \item Time-dependent baseline hazard \(h_0(t)\)
    \item Time-independent and linear hazard contribution
    \item All individuals have the \alert{same baseline hazard},\\ but a \alert{unique scaling factor}
    \item<2>[] \[g(\mathbf{x}) = \exp(b_1 x_1 + b_2 x_2 + \ldots + b_m x_m)\]
    \item<2> Obtain \(b_m\) by multivariate linear regression
  \end{itemize}
\end{frame}

\begin{frame}{Baselines \Romannum{2}}

    \begin{itemize}
        \item Cox Proportional Hazards assumption often violated in real-world datasets
        \item Relaxation approaches:
        \begin{enumerate}
            \item<2->Replace $g(\mathbf{x})$ to allow for \alert{non-linear interactions}
            \item<2->[] \textbf{\deepsurv{}} \cite{Katzman2018} uses a neural network encoder
            \item<3-> Relax scaling factor to $g(\mathbf{x}, t)$ to allow  \alert{time-dependent} survival curves
            \item<3->[] Consequently allows for crossing survival curves
            \item<3->[] \textbf{\coxtime{}} \cite{Kvamme2019} adds $t$ as an additional input
        \end{enumerate}
    \end{itemize}
\end{frame}

\begin{frame}{DCS --- Objective Function}
\begin{description}
    \item[Calibration] Punish (MSE) predictions $<1$ before event/censoring time and $>0$ after the event time
    \[\mathcal{L}_{\text {RPS}} = \sum_{i=1}^{n} \Bigg[ d_{i} \cdot \sum_{l=1}^{L}\left(\hat{S}\left(t_l \mid \mathbf{x}_{i}\right)-\mathbbm{1}_{l<l_i}\right)^{2} + \left(1 - d_{i}\right) \cdot \sum_{l=1}^{l_i}\left(\hat{S}\left(t_l \mid \mathbf{x}_{i}\right)-1\right)^{2} \Bigg]\]
    \item<2->[Discrimination] "Push away" predictions of individuals that survived longer
    \[\mathcal{\tilde{L}}_{\text {kernel}} = \sum_{i=1}^n \sum_{j=1}^n B_{i, j} \cdot \exp \left[-\frac{1}{\sigma}\left(\hat{S}\left(z_{i} \mid \mathbf{x}_{j}\right)-\hat{S}\left(z_{i} \mid \mathbf{x}_{i}\right)\right)\right]\]
\end{description}
\end{frame}

\begin{frame}{Discrimination Performance --- \cindexabb{} ($\uparrow$)}

\begin{figure}
    \centering
    \begin{tikzpicture}
        \node<1-3> (cindex) at (0,0) {\includegraphics[width=\textwidth]{img/scores_c-index_test.png}};
        \draw<-1>[draw=mLightBackground, fill=mLightBackground] (-2.3,-2) rectangle ++(10,6);
        \draw<-2>[draw=mLightBackground, fill=mLightBackground] (2.35,-2) rectangle ++(10,6);
    \end{tikzpicture}
\end{figure}

\end{frame}

\end{document}
